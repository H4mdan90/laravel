<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(){
    	return "ini index page book";
    }

    public function viewJudul($judul){
    	return "Buku yang anda baca berjudul : ". $judul;
    }
}
