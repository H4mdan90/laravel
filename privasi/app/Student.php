<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    //protected $table = "mahasiswa";  -> maping manual jika nama table bukan nama jamak dari nama model
    //protected $primaryKey = "mahasiswa_id"; -> maping manual jika primay key bukan defaultnya

    protected $fillable = ['nama', 'nrp', 'email', 'jurusan'];  // field mana aja yang boleh diisi
    // protected $guarded = ['id'];   // field mana aja yang TIDAK boleh diisi    

    use SoftDeletes;
}
