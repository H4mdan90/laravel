@extends('layout.main')	<!-- bisa pake . or / -->

@section('title', 'Student Lists')

@section('container')
<div class="container">
	<div class="row">
		<div class="col-6">
			<h1 class="mt-3">Student Detail</h1>
            
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{$student->nama}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">{{$student->nrp}}</h6>
                  <p class="card-text">{{$student->email}}</p>
                  <p class="card-text">{{$student->jurusan}}</p>
                  <a href="{{ url('/students/'.$student->id.'/edit') }}" class="btn btn-primary">edit</a>
                  <form class="d-inline" action="{{ $student->id }}" method="POST">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete this?')">delete</button>
                  </form>
                <a href="{{ url('/students') }}" class="btn btn-info">back</a>
                </div>
            </div>
		</div>
	</div>
</div>    
@endsection
