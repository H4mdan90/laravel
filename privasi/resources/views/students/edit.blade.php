@extends('layout.main')	<!-- bisa pake . or / -->

@section('title', 'Form Edit Data Student')

@section('container')
<div class="container">
	<div class="row">
		<div class="col-8">
			<h1 class="mt-3">Form Edit Data Student</h1>
            
            <form method="POST" action="{{ url('students/'.$student->id) }}">
                @method('patch')
                @csrf
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="input nama" name="nama" value="{{ old('nama',$student->nama) }}">
                  @error('nama')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="nrp">NRP</label>
                  <input type="text" class="form-control @error('nrp') is-invalid @enderror" id="nrp" placeholder="input nrp" name="nrp" value="{{ old('nrp',$student->nrp) }}">
                  @error('nrp')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="input email" name="email" value="{{ old('email',$student->email) }}">
                  @error('email')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="jurusan">Jurusan</label>
                  <input type="text" class="form-control @error('jurusan') is-invalid @enderror" id="jurusan" placeholder="input jurusan" name="jurusan" value="{{ old('jurusan',$student->jurusan) }}">
                  @error('jurusan')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit Data</button>
                <a href="{{ url('/students/'.$student->id) }}" class="btn btn-info">back</a>
              </form>
		</div>
	</div>
</div>    
@endsection
