<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/about', function () {
// 	$nama = 'Hamdan';
//     return view('about', ['nama' => $nama]);
// });

Route::get('/','PagesController@home');

Route::get('/about','PagesController@about');

Route::get('/mahasiswa','MahasiswaController@index');

// students
// Route Manual
// Route::get('/students','StudentsController@index');                 // view list data
// Route::get('/students/create','StudentsController@create');         // view form add data
// Route::get('/students/{student}','StudentsController@show');        // view detail data
// Route::post('/students','StudentsController@store');                // create / insert data
// Route::delete('/students/{student}','StudentsController@destroy');  // delete data
// Route::get('/students/{student}/edit','StudentsController@edit');   // view form edit data
// Route::patch('/students/{student}','StudentsController@update');   // update data

// Route otomatis untuk mengcover semua route yang sudah dibuat manual
Route::resource('students', 'StudentsController');


Route::get('book','BookController@index');
Route::get('book/{judul}','BookController@viewJudul');